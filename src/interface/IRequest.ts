import { Request } from "express"

interface User {
  email: string
  role: string
  iat: number
}

export interface IRequest extends Request {
  token: string,
  user: User
}