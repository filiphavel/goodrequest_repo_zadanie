import { Response, NextFunction } from 'express'
import { IRequest } from '../interface/IRequest'
import { KEY } from '../utils/key_enum'
import { translateService } from '../service/translation'
const jwt = require("jsonwebtoken")

async function verifyToken(_req:IRequest, res:Response, _next:NextFunction) {
    // get auth header value
    const token = _req.header("auth-token")
    if(!token){
        return res.status(400).send({
            msg: await translateService("Access denied!", _req.header("language"))
        })
    }
    try {
        const verified = jwt.verify(token, KEY.SECRET)
        _req.user = verified
        _next();
    } catch (error) {
        res.status(400).json({
            msg: await translateService("Invalid token!!", _req.header("language"))
        })
    }
}

export { verifyToken }