import { Response, NextFunction } from 'express'
import { IRequest } from '../interface/IRequest';
import { translateService } from '../service/translation';
import { models } from '../db'; 
const bcrypt = require('bcryptjs')

const { User, Credential } = models
const emailRegEx = new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}');

async function validateLogin(_req:IRequest, res:Response, _next:NextFunction) {
    const {email, password}  = _req.body

    try {
        // check email format and password
        if(!emailRegEx.test(email) || password.length < 6) {
            return res.status(400).send({
                msg: await translateService("Some field has invalid value!", _req.header("language"))
            })
        } 
    
        const user = await User.findOne({where: {email: email}})
        // check if email is registered
        if(user == null) {
            return res.status(400).send({
                msg: await translateService("Email is not registered!", _req.header("language"))
            })
        }
    
        // look for hashed password for account in database
        const credential = await Credential.findOne({where: {id: user.dataValues.id}})
        // check if credential is not null
        if(credential == null) {
            return res.status(400).send({
                msg: await translateService("Error occurred while logging in!", _req.header("language"))
            })
        }
        // check password
        const validPassword = await bcrypt.compare(password, credential.dataValues.password)
        if(!validPassword){
            return res.status(400).send({
                msg: await translateService("Invalid password!", _req.header("language"))
            })
        }
        _next()
        
    } catch (err) {
        console.error(err)
        return res.status(500).json({
            msg: await translateService("Something went wrong!", _req.header("language"))
        })
    }
}

export { validateLogin }