import { Response, NextFunction } from 'express'
import { IRequest } from '../interface/IRequest';
import { translateService } from '../service/translation';
import { models } from '../db'; 


const { Exercise } = models


async function filter(_req:IRequest, res:Response, _next:NextFunction) {
    const query = _req.query
    
    try {
        if(query.programID){
            // check if query is number
            if(Number.isNaN(query.programID)){
                return res.status(400).json({
                    msg: await translateService("Wronog query!", _req.header("language"))
                })
            }
            const loadExercises = await Exercise.findAll({
                where: {
                  programID: query.programID
                }
            })
            // check if loaded
            if(loadExercises !== null){
                return res.status(200).json({
                    msg: await translateService(`List of Program ${query.programID} exercises!`, _req.header("language")),
                    exercises: loadExercises
                });
            } else {
                return res.status(200).json({
                    msg: await translateService("No exercises yet!", _req.header("language"))
                });
            }
        } else {
            _next()
        }
    } catch (err) {
        console.error(err)
        return res.status(500).json({
            msg: await translateService("Something went wrong!", _req.header("language"))
        })
    }

    

    
}

export { filter }