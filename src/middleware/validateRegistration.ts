import { Response, NextFunction } from 'express'
import { IRequest } from '../interface/IRequest'
import { translateService } from '../service/translation'
import { models } from '../db'
import { USER_ROLE } from '../utils/user_role_enum'
const bcrypt = require('bcryptjs')


const {
	User
} = models

const emailRegEx = new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}');
const roles = [USER_ROLE.ADMIN, USER_ROLE.USER]

async function validateRegistration(_req:IRequest, res:Response, _next:NextFunction) {
    const {email, password, role}  = _req.body

    try {
        
            // check email format, password min len, check role
        if(!emailRegEx.test(email) || password.length < 6 || !roles.includes(role)) {
            return res.status(400).send({
                msg: await translateService("Some field has invalid value!", _req.header("language"))
            })
        } 
        const user = await User.findOne({where: {email: email}})
        // check if email is registered
        if(user !== null) {
            return res.status(400).send({
                msg: await translateService("Email already registered!", _req.header("language"))
            })
        }
        
        // hash password
        const salt = await bcrypt.genSalt(10)
        const hashedPassword = await bcrypt.hash(password, salt)
    
        _req.body.password = hashedPassword
    
        _next()
        
    } catch (err) {
        console.error(err)
        return res.status(500).json({
            msg: await translateService("Something went wrong!", _req.header("language"))
        })
    }
}

export { validateRegistration }