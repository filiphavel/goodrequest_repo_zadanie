import { Response, NextFunction } from 'express'
import Sequelize from 'sequelize'
import { IRequest } from '../interface/IRequest'
import { translateService } from '../service/translation';
import { models } from '../db'; 


const { Exercise } = models


async function search(_req:IRequest, res:Response, _next:NextFunction) {
    const query = _req.query
    try {
        if(query.search){
            const searchExercises = await Exercise.findAll({
                where: {
                    name: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('name')), 'LIKE', '%' + query.search + '%')
                }
            })
            // check if found
            if(searchExercises !== null){
                return res.status(200).json({
                    msg: await translateService("Search found!", _req.header("language")),
                    exercises: searchExercises
                });
            } else {
                return res.status(400).json({
                    msg: await translateService(`No exercises contains ${query.search}`, _req.header("language"))
                });
            }
        } else {
            _next()
        }
    } catch (err) {
        console.error(err)
        return res.status(500).json({
            msg: await translateService("Something went wrong!", _req.header("language"))
        })
    }
   

    
}

export { search }