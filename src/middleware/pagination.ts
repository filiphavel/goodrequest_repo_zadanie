import { Response, NextFunction } from 'express'
import { IRequest } from '../interface/IRequest';
import { translateService } from '../service/translation';
import { models } from '../db'; 


const { UserExercises } = models


async function pagination(_req:IRequest, res:Response, _next:NextFunction) {
    const query = _req.query

    try {
        if(!query.limit){
            query['limit'] = "2" // if limit is not in query, add limit to query with value
        }
    
        if(query.page){
            // check if queries are numbers
            if(Number.isNaN(query.page) && Number.isNaN(query.limit)){
                return res.status(400).json({
                    msg: await translateService("Wrong query!", _req.header("language"))
                })
            }
    
            const loadExercises = await UserExercises.findAndCountAll({
                limit: +query.limit, 
                offset: +query.page * +query.limit,
                where: {
                  email: _req.user.email
                }
            })
    
            // check if loaded
            if(loadExercises !== null){
                return res.status(200).json({
                    msg: await translateService("List of users exercises!", _req.header("language")),
                    totalExercises: loadExercises.count,
                    totalPages: Math.ceil(loadExercises.count / +query.limit),
                    currentPage: +query.page + 1,
                    exercises: loadExercises.rows
                });
            } else {
                return res.status(400).json({
                    msg: await translateService("rNo exercises yet!", _req.header("language"))
                });
            }
        } else {
            _next()
        }
        
    } catch (err) {
        console.error(err)
        return res.status(500).json({
            msg: await translateService("Something went wrong!", _req.header("language"))
        })
    }


    
}

export { pagination }