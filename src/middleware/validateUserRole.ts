import { Response, NextFunction } from 'express'
import { IRequest } from '../interface/IRequest'
import { translateService } from '../service/translation'
import { USER_ROLE } from '../utils/user_role_enum'


async function isAdmin(_req:IRequest, res:Response, _next:NextFunction) {
    
    if(_req.user.role == USER_ROLE.ADMIN){
        _next()
    } else {
        return res.status(400).json({
            msg: await translateService("You don't have permission!", _req.header("language"))
        })
    }
}

export { isAdmin }