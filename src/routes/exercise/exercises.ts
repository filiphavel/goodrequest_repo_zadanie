import { Router, Request, Response, NextFunction } from 'express'
import { verifyToken } from '../../middleware/verifyToken'
import { models } from '../../db'
import { translateService } from '../../service/translation'

const router: Router = Router()

const {
	Exercise,
	Program
} = models

export default () => {
	router.get('/', verifyToken, async (_req: Request, res: Response, _next: NextFunction) => {
		try {
			const exercises = await Exercise.findAll({
				include: [{
					model: Program,
					as: 'program'
				}]
			})
			return res.status(200).json({
				data: exercises,
				msg: await translateService('List of exercises!', _req.header("language"))
			})
		} catch (err) {
			console.error(err)
			return res.status(500).json({
				msg: await translateService("Something went wrong!", _req.header("language"))
			})
		}
	})

	return router
}
