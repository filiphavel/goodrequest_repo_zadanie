import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { isAdmin } from '../../middleware/validateUserRole'
import { verifyToken } from '../../middleware/verifyToken'
import { models } from '../../db'

const router: Router = Router()

const {
	Exercise
} = models

export default () => {
	router.put('/', [verifyToken ,isAdmin], async (_req: IRequest, res: Response, _next: NextFunction) => {
        const query = _req.query

		try {
			if(_req.body.difficulty){
				_req.body.difficulty = _req.body.difficulty.toUpperCase()
			}
			// update exercise
			const updateExercise = await Exercise.update(
				_req.body,
				{ where: { id: query.id } 
			})
	
			// check if updated
			if(updateExercise[0] == 1){
				return res.status(200).json({
					msg: await translateService("Exercise successfully updated!", _req.header("language"))
				})
			} else {
				return res.status(400).json({
					msg: await translateService("Error while update!", _req.header("language"))
				})
			}
			
		} catch (err) {
			console.error(err)
			return res.status(500).json({
				msg: await translateService("Something went wrong!", _req.header("language"))
			})
		}
      

	})

	return router
}
