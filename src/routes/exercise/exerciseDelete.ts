import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { isAdmin } from '../../middleware/validateUserRole'
import { verifyToken } from '../../middleware/verifyToken'
import { models } from '../../db'
import { translateService } from '../../service/translation'

const router: Router = Router()

const {
	Exercise
} = models

export default () => {
	router.delete('/', [verifyToken ,isAdmin], async (_req: IRequest, res: Response, _next: NextFunction) => {
        const query = _req.query
		try {
			// delete exercise
			const deleteExercise = await Exercise.destroy({
				where: { id: query.id }
			})
			
			// check if deleted
			if(deleteExercise == 1){
				return res.status(200).json({
					msg: await translateService('Exercise successfully deleted!', _req.header("language"))
				})
			} else {
				return res.status(400).json({
					msg: await translateService('Nothing to delete!', _req.header("language"))
				})
			}
			
		} catch (err) {
			console.error(err)
			return res.status(500).json({
				msg: await translateService("Something went wrong!", _req.header("language"))
			})
		}
      

	})

	return router
}
