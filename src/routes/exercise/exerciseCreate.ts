import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { isAdmin } from '../../middleware/validateUserRole'
import { verifyToken } from '../../middleware/verifyToken'
import { models } from '../../db'

const router: Router = Router()

const {
	Exercise
} = models

export default () => {
	router.post('/', [verifyToken ,isAdmin], async (_req: IRequest, res: Response, _next: NextFunction) => {
        const {name, difficulty, programID} = _req.body

        try {
            // check if exercise exists
            const findExercise = await Exercise.findOne({where:{name: name}})
            if(findExercise !== null){
                return res.status(400).json({
                    msg: await translateService("Exercise already exists!", _req.header("language"))
                })
            }
            // create new exercise
            const saveExercise = await Exercise.create({
                name: name,
                difficulty: difficulty,
                programID: programID
            })
            
            // check if created
            if(saveExercise !== null){
                return res.status(200).json({
                    data: {
                        name: name,
                    },
                    msg: await translateService("Exercise successfully created!", _req.header("language"))
                })
            } else {
                return res.status(400).json({
                    msg: await translateService("Error while creating exercise!", _req.header("language"))
                })
            }
            
        } catch (err) {
            console.error(err)
            return res.status(500).json({
                msg: await translateService("Something went wrong!", _req.header("language"))
            })
        }

	})

	return router
}
