import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { verifyToken } from '../../middleware/verifyToken'
import { models } from '../../db'

const router: Router = Router()

const {
	Program
} = models

export default () => {
	router.get('/', verifyToken, async (_req: IRequest, res: Response, _next: NextFunction) => {
		try {
			const programs = await Program.findAll()
			return res.json({
				data: programs,
				msg: await translateService("List of programs!", _req.header("language"))
			})
			
		} catch (err) {
			console.error(err)
			return res.status(500).json({
				msg: await translateService("Something went wrong!", _req.header("language"))
			})
		}
	})

	return router
}
