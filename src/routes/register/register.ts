import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { models } from '../../db'
import { validateRegistration } from '../../middleware/validateRegistration'

const router: Router = Router()

const {
	User,
    Credential
} = models

export default () => {
	router.post('/', validateRegistration, async (_req: IRequest, res: Response, _next: NextFunction) => {
        const {email, password, role } = _req.body 

        try {
            // save User to database
            const saveUser = await User.create({
                email: email,
                role: role
            })
            // save password to database
            const saveUserCredential = await Credential.create({
                password: password,
                userID: parseInt(saveUser.dataValues.id)
            })
            // check if created
            if(saveUser !== null && saveUserCredential !== null){
                return res.status(200).json({
                    msg: await translateService("User successfully registered!", _req.header("language")),
                    data: {
                        email: email,
                        role: role
                    }
                });
            } else {
                return res.status(400).send({
                    msg: await translateService("registration failed!", _req.header("language"))
                });
            }
            
        } catch (err) {
            console.error(err)
            return res.status(500).json({
                msg: await translateService("Something went wrong!", _req.header("language"))
            })
        }

	})

	return router
}
