import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { verifyToken } from '../../middleware/verifyToken'
import {isAdmin} from '../../middleware/validateUserRole'
import { models } from '../../db'

const router: Router = Router()

const {
	User
} = models

export default () => {
	router.get('/', [verifyToken, isAdmin], async (_req: IRequest, res: Response, _next: NextFunction) => {
		const query = _req.query

        // check if query is not empty
        if(Object.keys(_req.query).length === 0 || query.email == '' || query.email == null){
            return res.status(400).json({
                msg: await translateService("Query must not be empty!", _req.header("language"))
            })
        }

        try {  
            const user = await User.findOne({where: {email: query.email}})
            // check if user found
    
            if(user != null){
                return res.status(200).json({
                    msg: await translateService("User info!", _req.header("language")),
                    data: user.dataValues
                })
    
            } else {
                return res.status(200).json({ 
                    msg: await translateService("User not found!", _req.header("language"))
                })
            }
        } catch (err) {
            console.error(err)
            return res.status(500).json({
                msg: await translateService("Something went wrong!", _req.header("language"))
            })
        }
		
	})

	return router
}
