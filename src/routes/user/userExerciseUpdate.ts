import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { models } from '../../db'
import { verifyToken } from '../../middleware/verifyToken'

const router: Router = Router()

const {
	UserExercises
} = models

export default () => {
	router.put('/', verifyToken, async (_req: IRequest, res: Response, _next: NextFunction) => {
        const query = _req.query

        if(Object.keys(_req.query).length === 0 || query.exerciseID == '' || query.exerciseID == null){
            return res.status(400).json({
                msg: await translateService("Query must not be empty!", _req.header("language"))
            })
        }

        if(Number.isNaN(query.exerciseID)){
            return res.status(400).json({
                msg: await translateService("Wrong query id!", _req.header("language"))
            })
        }

        try {
            const exercise = await UserExercises.findOne({where: {id: _req.query.exerciseID}})
            if(exercise !== null){
                let time = new Date()
                let start = exercise.dataValues.createdAt.getTime()
                let end = time.getTime()
                let result = (end - start)
                let resultSeconds = result / 1000
                _req.body['duration'] = resultSeconds
                // save exercise of user to database
                const saveUser = await UserExercises.update(
                    _req.body,
                    { where: { id: query.exerciseID } 
                })
        
                // check if created
                if(saveUser !== null){
                    return res.status(200).json({
                        msg: await translateService("Exercise ended!", _req.header("language"))
                    });
                } else {
                    return res.status(200).json({
                        msg: await translateService("Exercise ended!", _req.header("language"))
                    });
                }
            } else {
                return res.status(200).json({
                    msg: await translateService("Exercise you try to end des not exist!", _req.header("language"))
                });
            }
        } catch (err) {
            console.error(err)
            return res.status(500).json({
                msg: await translateService("Something went wrong!", _req.header("language"))
            })
        }

        

	})

	return router
}
