import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { models } from '../../db'
import { verifyToken } from '../../middleware/verifyToken'
import { pagination } from '../../middleware/pagination'
import { filter } from '../../middleware/filter'
import { search } from '../../middleware/search'

const router: Router = Router()

const {
	UserExercises
} = models

export default () => {
	router.get('/', [verifyToken, pagination, filter, search], async (_req: IRequest, res: Response, _next: NextFunction) => {
        try {
            // load exercise of user
            const loadExercises = await UserExercises.findAll({
                where: {
                  email: _req.user.email
                }
            })
    
            // check if loaded
            if(loadExercises !== null){
                return res.status(200).json({
                    msg: await translateService("List of user exercises!", _req.header("language")),
                    data: loadExercises
                });
            } else {
                return res.status(200).json({
                    msg: await translateService("No exercises yet!", _req.header("language"))
                });
            }
        } catch (err) {
            console.error(err)
            return res.status(500).json({
                msg: await translateService("Something went wrong!", _req.header("language"))
            })
        }
        

	})

	return router
}
