import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { verifyToken } from '../../middleware/verifyToken'
import {isAdmin} from '../../middleware/validateUserRole'
import { models } from '../../db'

const router: Router = Router()

const {
	User
} = models

export default () => {
	router.put('/', [verifyToken, isAdmin], async (_req: IRequest, res: Response, _next: NextFunction) => {
		const query = _req.query

        if(Object.keys(_req.query).length === 0 || query.email == '' || query.email == null){
            return res.status(200).json({
				msg: await translateService("Query must not be empty!", _req.header("language"))
            })
        }
        
		try {
			// update user
			const updateUser = await User.update(
				_req.body,
				{ where: { email: query.email } 
			})
	
			// check if updated
			if(updateUser[0] == 1){
				return res.status(200).json({
					msg: await translateService("User successfully updated!", _req.header("language"))
				})
			} else {
				return res.status(400).json({
					msg: await translateService("Error while update!", _req.header("language"))
				})
			}
		} catch (err) {
			console.error(err)
			return res.status(500).json({
				msg: await translateService("Something went wrong!", _req.header("language"))
			})
		}
		
	})

	return router
}
