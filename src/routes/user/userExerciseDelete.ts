import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { verifyToken } from '../../middleware/verifyToken'
import { models } from '../../db'

const router: Router = Router()

const {
	UserExercises
} = models

export default () => {
	router.delete('', verifyToken, async (_req: IRequest, res: Response, _next: NextFunction) => {
        const query = _req.query

        if(Object.keys(_req.query).length === 0 || query.id == '' || query.id == null){
            return res.status(400).json({
				msg: await translateService("Query must not be empty!", _req.header("language"))
            })
        }

        if(Number.isNaN(query.id)){
            return res.status(400).json({
				msg: await translateService("Wrong query id!", _req.header("language"))
            })
        }

		try {
			// delete user exercise
			const deleteExercise = await UserExercises.destroy({
				where: { email: _req.user.email, id: query.id }
			})
			
			// check if deleted
			if(deleteExercise == 1){
				return res.status(200).json({
					msg: await translateService("Exercise successfully deleted!", _req.header("language"))
				})
			} else {
				return res.status(400).json({
					msg: await translateService("Nothing to delete!", _req.header("language"))
				})
			}
			
		} catch (err) {
			console.error(err)
			return res.status(500).json({
				msg: await translateService("Something went wrong!", _req.header("language"))
			})
		}

      

	})

	return router
}
