import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { verifyToken } from '../../middleware/verifyToken'
import { models } from '../../db'

const router: Router = Router()

const {
	User
} = models

export default () => {
	router.get('/', verifyToken, async (_req: IRequest, res: Response, _next: NextFunction) => {
		const query = _req.query

        // check if query is not empty
        if(Object.keys(_req.query).length == 0 || query.email == '' || query.email == null){
            return res.status(400).json({
                msg: "Query must not be empty!"
            })
        }

        try {
            // check if user is requesting his own profile data
            if(query.email == _req.user.email){          
                const user = await User.findOne({where: {email: query.email}, attributes: ['name', 'surname', 'age', 'nickName']})
                // check if user found
        
                if(user != null){
                    return res.status(200).json({
                        msg: "User info",
                        data: user.dataValues
                    })
        
                } else {
                    return res.status(400).send({ msg: "Error while loading personal info!" })
                }
            } else {
                return res.status(400).json({
                    msg: "Access denied!"
                })
            }
        } catch (err) {
            console.error(err)
            return res.status(500).json({
                msg: await translateService("Something went wrong!", _req.header("language"))
            })
        }
		
	})

	return router
}
