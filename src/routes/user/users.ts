import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { verifyToken } from '../../middleware/verifyToken'
import { models } from '../../db'
import { USER_ROLE } from '../../utils/user_role_enum'

const router: Router = Router()

const {
	User
} = models

export default () => {
	router.get('/', verifyToken, async (_req: IRequest, res: Response, _next: NextFunction) => {
		try {
			let users;
			if(_req.user.role == USER_ROLE.ADMIN){
				// admin stuff
				users = await User.findAll()
			} else {
				// user stuff
				users = await User.findAll({attributes: ['id', 'nickName']})
			}
			
			return res.status(200).json({
				msg: await translateService("User details!", _req.header("language")),
				data: users,
				user: _req.user
			})
		} catch (err) {
			console.error(err)
			return res.status(500).json({
				msg: await translateService("Something went wrong!", _req.header("language"))
			})
		}
	})

	return router
}
