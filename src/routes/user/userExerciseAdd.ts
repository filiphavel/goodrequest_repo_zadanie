import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { models } from '../../db'
import { verifyToken } from '../../middleware/verifyToken'

const router: Router = Router()

const {
	UserExercises,
    Exercise
} = models

export default () => {
	router.post('/', verifyToken, async (_req: IRequest, res: Response, _next: NextFunction) => {

        try {
            const exercise = await Exercise.findOne({where: {id: _req.body.exerciseID}})
    
            if(exercise !== null){
                // save exercise of user to database
                const saveUser = await UserExercises.create({
                    email: _req.user.email,
                    state: "IN PROGRESS",
                    exercise: _req.body.exerciseID
                })
        
                // check if created
                if(saveUser !== null){
                    return res.status(200).json({
                        msg: await translateService("Exercise started!", _req.header("language"))
                    });
                } else {
                    return res.status(400).json({
                        msg: await translateService("Error while starting exercise!", _req.header("language"))
                    });
                }
            } else {
                return res.status(400).json({
                    msg: await translateService("Exercise you try to start does not exist!", _req.header("language"))
                });
            }
            
        } catch (err) {
            console.error(err)
            return res.status(500).json({
                msg: await translateService("Something went wrong!", _req.header("language"))
            })
        }
        

	})

	return router
}
