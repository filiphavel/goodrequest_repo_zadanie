import { Router, Response, NextFunction } from 'express'
import { IRequest } from '../../interface/IRequest'
import { translateService } from '../../service/translation'
import { models } from '../../db'
import { validateLogin } from '../../middleware/validateLogin'
import { KEY } from '../../utils/key_enum'
const jwt = require("jsonwebtoken")

const router: Router = Router()

const {
	User
} = models

export default () => {
	router.post('/', validateLogin, async (_req: IRequest, res: Response, _next: NextFunction) => {
        try {
            const user = await User.findOne({where: {email: _req.body.email}})
            if(user != null){
                let userInfo = {
                    email: _req.body.email,
                    role: user.dataValues.role
                }
    
                const token = jwt.sign(userInfo, KEY.SECRET)
                res.header("auth-token", token).json({token: token})
    
            } else {
                res.status(400).send({ 
                    msg: await translateService("Wrong login credentials!", _req.header("language"))
                })
            }
            
        } catch (err) {
            console.error(err)
            return res.status(500).json({
                msg: await translateService("Something went wrong!", _req.header("language"))
            })
        }
    
	})

	return router
}
