import http from 'http'
import express from 'express'
import * as bodyParser from 'body-parser'

import { sequelize } from './db'

import ProgramRouter from './routes/program/programs'

import UserRouter from './routes/user/users'
import UserSpecificRouter from './routes/user/userGet'
import UserUpdateRouter from './routes/user/userUpdate'
import UserProfileRouter from './routes/user/userProfileGet'
import UserProfileUpdateRouter from './routes/user/userProfileUpdate'
import UserExerciseAddRouter from './routes/user/userExerciseAdd'
import UserExercisesLoadRouter from './routes/user/userExercisesLoad'
import UserExerciseUpdateRouter from './routes/user/userExerciseUpdate'
import UserExercisesCompletedLoadRouter from './routes/user/userExercisesCompletedLoad'
import UserExerciseDeleteRouter from './routes/user/userExerciseDelete'

import LoginRoter from './routes/login/login'
import RegisterRouter from './routes/register/register'

import ExerciseRouter from './routes/exercise/exercises'
import ExerciseCreateRouter from './routes/exercise/exerciseCreate'
import ExerciseDeleteRouter from './routes/exercise/exerciseDelete'
import ExerciseUpdateRouter from './routes/exercise/exerciseUpdate'

const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// routes
app.use('/api/programs', ProgramRouter()) // GET all programs
app.use('/api/exercises', ExerciseRouter()) // GET all exercises

// user routes
app.use('/api/users', UserRouter()) // GET all users
app.use('/api/user/myprofile', UserProfileRouter()) // GET users own profile information
app.use('/api/user/myprofile/update', UserProfileUpdateRouter()) // UPDATE users own profile information

// login and register routes
app.use('/api/login', LoginRoter()) // login user
app.use('/api/register', RegisterRouter()) // register user

// user exercise routes
app.use('/api/user/load/exercises', UserExercisesLoadRouter()) // LOAD exercises of user, pagination, filter,search
app.use('/api/user/load/completed/exercises', UserExercisesCompletedLoadRouter()) // LOAD completed exercises of user
app.use('/api/user/add/exercise', UserExerciseAddRouter()) // ADD new exercise to user
app.use('/api/user/update/exercise', UserExerciseUpdateRouter()) // UPDATE exercise to user
app.use('/api/user/delete/exercise', UserExerciseDeleteRouter()) // DELETE exercise from user

// only admin routes

// exercise
app.use('/api/exercise/create', ExerciseCreateRouter()) // CREATE exercise
app.use('/api/exercise/delete', ExerciseDeleteRouter()) // DELETE exercise
app.use('/api/exercise/update', ExerciseUpdateRouter()) // UPDATE exercise

// user
app.use('/api/user', UserSpecificRouter()) // GET specific user
app.use('/api/user/update', UserUpdateRouter()) // UPDATE user

const httpServer = http.createServer(app)

sequelize.sync()

console.log('Sync database', 'postgresql://localhost:5432/fitness_app')

httpServer.listen(8000).on('listening', () => console.log(`Server started at port ${8000}`))

export default httpServer
