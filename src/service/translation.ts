import { KEY } from "../utils/key_enum"
const translate = require("translate")

translate.engine = "deepl"
translate.key = KEY.DEEPL

async function translateService(text:string, lang:string) {
    try {
        if(lang){
            const translation = await translate(text, lang)
            return String(translation)
        }
        return text
        
    } catch (err) {
        console.error(err)
        return text
    }
}

export { translateService }