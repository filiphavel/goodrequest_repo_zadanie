/* eslint import/no-cycle: 0 */

import { Sequelize, DataTypes } from 'sequelize'
import { DatabaseModel } from '../types/db'

export class UserExerciseModel extends DatabaseModel {
	id: number
    email: String
    state: String
    duration: String
    exercise: number
}

export default (sequelize: Sequelize) => {
	UserExerciseModel.init({
		id: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			allowNull: false,
			autoIncrement: true
		},
        email: {
            type: DataTypes.STRING(30),
            validate: {
                isEmail: true // validate email format
            }
        },
        state: {
            type: DataTypes.STRING(11),
            validate: {
                isIn: [['DONE', 'IN PROGRESS']] // accept only this values
            }
        },
        duration: {
            type: DataTypes.STRING(8)
        },
        exercise: {
            type: DataTypes.INTEGER
        }
	}, {
		timestamps: true,
		sequelize,
		modelName: 'userExercise'
	})

	return UserExerciseModel
}
