/* eslint import/no-cycle: 0 */

import { Sequelize, DataTypes } from 'sequelize'
import { DatabaseModel } from '../types/db'
import { UserModel } from './user'

export class CredentialModel extends DatabaseModel {
	id: number
	password: String

	user: UserModel
}

export default (sequelize: Sequelize) => {
	CredentialModel.init({
		id: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			allowNull: false,
			autoIncrement: true
		},
		password: {
			type: DataTypes.STRING(100),
            validate: {
                len: [6, 100]
            }
		}
	}, {
		timestamps: true,
		sequelize,
		modelName: 'credential'
	})

	CredentialModel.associate = (models) => {
		(CredentialModel as any).belongsTo(models.User, {
			foreignKey: {
				name: 'userID',
				allowNull: false
			},
			as: 'credential'
		})
	}

	return CredentialModel
}
