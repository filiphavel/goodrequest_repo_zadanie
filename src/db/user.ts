/* eslint import/no-cycle: 0 */

import { Sequelize, DataTypes } from 'sequelize'
import { DatabaseModel } from '../types/db'
import { USER_ROLE } from '../utils/user_role_enum'
import { CredentialModel } from './credential'

export class UserModel extends DatabaseModel {
	id: number
	name: String
    surname: String
    nickName: String 
    email: String
    age: number
    role: USER_ROLE

    credential: CredentialModel
}

export default (sequelize: Sequelize) => {
	UserModel.init({
		id: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			allowNull: false,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(15)
		},
        surname: {
            type: DataTypes.STRING(15)
        },
        nickName: {
            type: DataTypes.STRING(20)
        },
        email: {
            type: DataTypes.STRING(30),
            validate: {
                isEmail: true // validate email format
            }
        },
        age: {
            type: DataTypes.INTEGER
        },
        role: {
            type: DataTypes.ENUM(...Object.values(USER_ROLE)),
            validate: {
                isIn: [['ADMIN', 'USER']] // accept only ADMIN / USER value
            }
        }
	}, {
		timestamps: true,
		sequelize,
		modelName: 'user'
	})

	UserModel.associate = (models) => {
		(UserModel as any).hasOne(models.Credential, {
			foreignKey: {
				name: 'userID',
				allowNull: false
			},
		})
	}

	return UserModel
}
